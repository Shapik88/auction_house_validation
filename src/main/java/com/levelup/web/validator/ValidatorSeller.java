package com.levelup.web.validator;

import com.levelup.web.auctionHouse.Seller;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


@Component
public class ValidatorSeller implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Seller.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Seller seller = (Seller) o;
        String firstName = seller.getFirstName();
        String lastName = seller.getLastName();
        int licenseNumber = seller.getLicenseNumber();
        String nameLot = seller.getNameLot();
        String citySeller = seller.getCitySeller();
        if (firstName.equals("")) {
            errors.rejectValue("firstName", "400", "Not Null");
        }
        if (lastName.equals("")) {
            errors.rejectValue("lastName", "400", "Not Null");
        }
        if (nameLot.equals("")) {
            errors.rejectValue("nameLot", "400", "Not Null");
        }
        if (citySeller.equals("")) {
            errors.rejectValue("citySeller", "400", "Not Null");
        }
        if (licenseNumber < 0) {
            errors.rejectValue("licenseNumber", "400", "Not Null");
        }
    }
}
