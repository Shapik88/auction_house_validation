package com.levelup.web.validator;

import com.levelup.web.auctionHouse.Buyer;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by Евгений on 10.10.2017.
 */
@Component
public class ValidatorBuyer implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Buyer.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Buyer buyer = (Buyer) o;
        String firstName = buyer.getFirstName();
        String lastName = buyer.getLastName();
        int licenseNumber = buyer.getLicenseNumber();
        String cityBuyer = buyer.getCityBuyer();
        if (firstName.equals("")) {
            errors.rejectValue("firstName", "400", "Not Null");
        }
        if (lastName.equals("")) {
            errors.rejectValue("lastName", "400", "Not Null");
        }
        if (cityBuyer.equals("")) {
            errors.rejectValue("cityBuyer", "400", "Not Null");
        }
        if (licenseNumber < 0) {
            errors.rejectValue("licenseNumber", "400", "Not Null");
        }
    }
}
