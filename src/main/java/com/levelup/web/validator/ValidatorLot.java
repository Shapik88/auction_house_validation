package com.levelup.web.validator;

import com.levelup.web.auctionHouse.Lot;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


@Component
public class ValidatorLot implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Lot.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Lot lot = (Lot) o;
        String sellerName = lot.getSellerName();
        if (sellerName.equals("")) {
            errors.rejectValue("sellerName", "400", "Not Null");
        }
        String nameLot = lot.getNameLot();
        if (nameLot.equals("")) {
            errors.rejectValue("nameLot", "400", "Not Null");
        }
    }
}
