package com.levelup.web.auctionHouse;

import lombok.Data;


@Data
public class Buyer {

    private String firstName;
    private String lastName;
    private int licenseNumber;
    private String cityBuyer;
}
