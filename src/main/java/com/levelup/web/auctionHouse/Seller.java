package com.levelup.web.auctionHouse;

import lombok.Data;

@Data
public class Seller {


    private String firstName;
    private String lastName;
    private int licenseNumber;
    private String nameLot;
    private String citySeller;
}
