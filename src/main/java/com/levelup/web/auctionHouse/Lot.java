package com.levelup.web.auctionHouse;

import lombok.Data;

@Data
public class Lot {


    private String nameLot;

    private String sellerName;

}
