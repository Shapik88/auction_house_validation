package com.levelup.web.auctionHouse;

import lombok.Data;


@Data
public class LoginAndPassword {

    private String email;
    private String password;


}
