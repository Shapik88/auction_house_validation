package com.levelup.web;

import com.levelup.web.auctionHouse.Buyer;
import com.levelup.web.auctionHouse.LoginAndPassword;
import com.levelup.web.auctionHouse.Lot;
import com.levelup.web.auctionHouse.Seller;
import com.levelup.web.validator.AllValidator;
import com.levelup.web.validator.ValidatorBuyer;
import com.levelup.web.validator.ValidatorLot;
import com.levelup.web.validator.ValidatorSeller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;


/**
 * Created by Евгений on 10.10.2017.
 */


@RestController
public class MainRestController {

    private AllValidator allValidator;
    private ValidatorBuyer validatorBuyer;
    private ValidatorSeller validatorSeller;
    private ValidatorLot validatorLot;


    @Autowired
    public MainRestController(AllValidator allValidator, ValidatorBuyer validatorBuyer,
                              ValidatorSeller validatorSeller, ValidatorLot validatorLot) {

        this.allValidator = allValidator;
        this.validatorBuyer = validatorBuyer;
        this.validatorSeller = validatorSeller;
        this.validatorLot = validatorLot;
    }

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(allValidator);

    }

    @InitBinder
    private void initBinderLot(WebDataBinder binder) {
        binder.setValidator(validatorLot);

    }

    @InitBinder
    private void initBinderSeller(WebDataBinder binder) {
        binder.setValidator(validatorSeller);

    }

    @InitBinder
    private void initBinderBuyer(WebDataBinder binder) {
        binder.setValidator(validatorBuyer);

    }


    @RequestMapping(value = "/form", method = RequestMethod.POST)
    public ResponseEntity<String> getFormPost(@RequestBody @Validated LoginAndPassword loginAndPassword,
                                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(bindingResult.getAllErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("; ")),
                    HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @RequestMapping(value = "/buyer", method = RequestMethod.POST)
    public ResponseEntity<String> getFormPost(@RequestBody @Validated Buyer buyer,
                                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(bindingResult.getAllErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("; ")),
                    HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @RequestMapping(value = "/seller", method = RequestMethod.POST)
    public ResponseEntity<String> getFormPost(@RequestBody @Validated Seller seller,
                                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(bindingResult.getAllErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("; ")),
                    HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @RequestMapping(value = "/lot", method = RequestMethod.POST)
    public ResponseEntity<String> getFormPostLot(@RequestBody @Validated Lot lot,
                                                 BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(bindingResult.getAllErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("; ")),
                    HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<String>(HttpStatus.OK);
    }


}
