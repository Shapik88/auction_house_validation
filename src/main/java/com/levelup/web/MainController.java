package com.levelup.web;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {

    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String getMain() {
        return "static/auction_house.html";
    }

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String getForm(@RequestParam String email,
                          @RequestParam String password) {

        return "/static/menu.html";
    }

    @RequestMapping(value = "/seller", method = RequestMethod.GET)
    public String getForSeller(@RequestParam String firstName,
                               @RequestParam String lastName,
                               @RequestParam int licenseNumber,
                               @RequestParam String nameLot,
                               @RequestParam String citySeller) {

        return "/static/menu.html";
    }

    @RequestMapping(value = "/buyer", method = RequestMethod.GET)
    public String getForBuyer(@RequestParam String firstName,
                              @RequestParam String lastName,
                              @RequestParam int licenseNumber,
                              @RequestParam String nameLot,
                              @RequestParam String cityBuyer) {

        return "/static/menu.html";
    }

    @RequestMapping(value = "/lot", method = RequestMethod.GET)
    public String getForSeller(@RequestParam String nameLot,
                               @RequestParam String sellerName) {

        return "/static/menu.html";
    }

}